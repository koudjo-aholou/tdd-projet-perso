/* eslint-disable no-new */
/* eslint-disable no-undef */
const { ThreeErrorsGame, Player } = require('./ThreeErrorsGame.js')

const Three_Errors_Game_ERR = new Error('ThreeErrorsGame can be filled only with object')
const Player_wrong_answer = new Error('Wrong answer !')
const Player_not_ready = new Error('The other player is not ready or connected')

const fakeData = {
  theme: 'animaux',
  sentences: {
    a: 'je déteste les chiens',
    b: 'j\'adore les chats',
    c: 'j\'ai eu une araignée domestique'
  },
  rightAnswer: 'j\'ai eu une araignée domestique'
}
const fakePlayer1 = { firstname: 'Tyler', score: 0, ready: false }
const fakePlayer2 = { firstname: 'Marla', score: 0, ready: false }

describe('Test the ThreeErrorsGame class', () => {
  test('ThreeErrorsGame::constructor(<obj>)', () => {
    const round = new ThreeErrorsGame(fakeData)
    expect(round).toBeInstanceOf(ThreeErrorsGame)
    expect(round).toHaveProperty('theme', 'animaux')
    expect(round).toHaveProperty('sentences')
    expect(round).toHaveProperty('rightAnswer', 'j\'ai eu une araignée domestique')
  })

  test('ThreeErrorsGame::constructor(<any>) // throw Error', () => {
    expect(() => { new ThreeErrorsGame() }).toThrow(Three_Errors_Game_ERR)
    expect(() => { new ThreeErrorsGame(undefined) }).toThrow(Three_Errors_Game_ERR)
    expect(() => { new ThreeErrorsGame(null) }).toThrow(Three_Errors_Game_ERR)
    expect(() => { new ThreeErrorsGame(1) }).toThrow(Three_Errors_Game_ERR)
    expect(() => { new ThreeErrorsGame(NaN) }).toThrow(Three_Errors_Game_ERR)
    expect(() => { new ThreeErrorsGame([]) }).toThrow(Three_Errors_Game_ERR)
    expect(() => { new ThreeErrorsGame(() => {}) }).toThrow(Three_Errors_Game_ERR)
  })
})

describe('Test the Player class', () => {
  test('Player::constructor(<obj>)', () => {
    const player1 = new Player(fakePlayer1)
    expect(player1).toBeInstanceOf(Player)
    expect(player1).toBeInstanceOf(ThreeErrorsGame)
    expect(player1).toHaveProperty('firstname', 'Tyler')
    expect(player1).toHaveProperty('score', 0)
    expect(player1).toHaveProperty('ready', false)
  })
  test('player.ready', () => {
    const player1 = new Player(fakePlayer1)
    expect(player1).toBeInstanceOf(Player)
    expect(player1).toBeInstanceOf(ThreeErrorsGame)
    expect(player1.ready).toBe(false)
    player1.ready = true
    expect(player1.ready).toBe(true)
    player1.checked = false
    expect(player1.checked).toBe(false)
  })

  test('player.toggle()', () => {
    const player1 = new Player(fakePlayer1)
    const player2 = new Player(fakePlayer2)
    expect(player1).toBeInstanceOf(Player)
    expect(player1).toBeInstanceOf(ThreeErrorsGame)
    expect(player2).toBeInstanceOf(Player)
    expect(player2).toBeInstanceOf(ThreeErrorsGame)
    expect(player1.ready).toBe(false)
    expect(player1.toggle()).toBe(true)
    expect(player1.ready).toBe(true)
    expect(player1.toggle()).toBe(false)
    expect(player1.ready).toBe(false)

    expect(player2.ready).toBe(false)
    expect(player2.toggle()).toBe(true)
    expect(player2.ready).toBe(true)
    expect(player2.toggle()).toBe(false)
    expect(player2.ready).toBe(false)
  })

  test('player.launchGame()', () => {
    const player1 = new Player(fakePlayer1)
    const player2 = new Player(fakePlayer2)
    expect(player1).toBeInstanceOf(Player)
    expect(player1).toBeInstanceOf(ThreeErrorsGame)
    expect(player2).toBeInstanceOf(Player)
    expect(player2).toBeInstanceOf(ThreeErrorsGame)
    expect(player1.ready).toBe(false)
    expect(player1.toggle()).toBe(true)
    expect(player1.ready).toBe(true)
    expect(player2.ready).toBe(false)
    expect(player2.toggle()).toBe(true)
    expect(player2.ready).toBe(true)

    expect(player1.launchGame(player2.ready)).toBe(true)
    expect(player1.launchTheGame).toBe(true)
    expect(player2.launchGame(player1.ready)).toBe(true)
    expect(player2.launchTheGame).toBe(true)
  })
  test('player1.launchGame() player2 not ready then ready', () => {
    const player1 = new Player(fakePlayer1)
    const player2 = new Player(fakePlayer2)
    expect(player1).toBeInstanceOf(Player)
    expect(player2).toBeInstanceOf(Player)
    expect(player1.ready).toBe(false)
    expect(player1.toggle()).toBe(true)
    expect(player1.ready).toBe(true)
    expect(player2.ready).toBe(false)
    expect(() => { player1.launchGame(player2.ready) }).toThrow(Player_not_ready)
    expect(player1.launchTheGame).toBe(false)
    expect(player2.toggle()).toBe(true)
    expect(player2.ready).toBe(true)
    expect(player1.ready).toBe(true)
    expect(player2.launchGame(player1.ready)).toBe(true)
    expect(player1.launchGame(player2.ready)).toBe(true)
    expect(player2.launchTheGame).toBe(true)
    expect(player1.launchTheGame).toBe(true)
  })
  test('Player 1 check answer', () => {
    const player1 = new Player(fakePlayer1)
    expect(player1).toBeInstanceOf(Player)
    expect(player1).toBeInstanceOf(ThreeErrorsGame)
    expect(player1).toHaveProperty('rightAnswer', 'j\'ai eu une araignée domestique')
    expect(player1.checkAnswer('j\'ai eu une araignée domestique')).toBe(true)
  })
  test('Player 1 answer success add 1 to score', () => {
    const player1 = new Player(fakePlayer1)
    expect(player1).toBeInstanceOf(Player)
    expect(player1).toBeInstanceOf(ThreeErrorsGame)
    expect(player1.score).toBe(0)
    expect(player1.addOnetoScore())
    expect(player1.score).toBe(1)
  })
})
