
class ThreeErrorsGame {
  constructor (data) {
    if (typeof data !== 'object' || data === null || Array.isArray(data)) {
      throw new Error('ThreeErrorsGame can be filled only with object')
    } else {
      this.theme = data.theme
      this.sentences = data.sentences
      this.rightAnswer = data.rightAnswer
    }
  }
}

// extend a faire pour recuperer la bonne reponse
class Player extends ThreeErrorsGame {
  constructor (dataPlayer) {
    super(dataPlayer)
    this.firstname = dataPlayer.firstname
    this.score = dataPlayer.score
    this.ready = dataPlayer.ready || false
    this.launchTheGame = false
  }

  toggle () {
    this.ready = !this.ready
    return this.ready
  }

  launchGame (player2) {
    if (this.ready === true && player2) {
      this.launchTheGame = true
      return this.launchTheGame
    } else {
      throw new Error('The other player is not ready or connected')
    }
  }

  checkAnswer (playerChoice) {
    if (playerChoice === this.rightAnswer) {
      return this.addOnetoScore()
    }
    throw Error('Wrong answer !')
  }

  addOnetoScore () {
    this.score += 1
    return this.score
  }
}

module.exports = { ThreeErrorsGame, Player }
